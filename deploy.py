# coding: utf-8
import os
import socket

master_nodes = {
    '172.16.15.1': 'es-master01',
    '172.16.16.1': 'es-master02'
}

data_nodes = {
    '172.16.13.2': 'es-data-132',
    '172.16.13.3': 'es-data-133',
    '172.16.13.4': 'es-data-134',
    '172.16.13.5': 'es-data-135',
    '172.16.13.6': 'es-data-136',
    '172.16.13.7': 'es-data-137',
    '172.16.14.1': 'es-data-141',
    '172.16.14.2': 'es-data-142',
    '172.16.14.3': 'es-data-143',
    '172.16.14.4': 'es-data-144',
    '172.16.14.5': 'es-data-145',
    '172.16.14.6': 'es-data-146',
    '172.16.14.7': 'es-data-147',
    '172.16.15.2': 'es-data-152',
    '172.16.15.3': 'es-data-153',
    '172.16.15.4': 'es-data-154',
    '172.16.15.5': 'es-data-155',
    '172.16.15.6': 'es-data-156',
    '172.16.15.7': 'es-data-157',
    '172.16.16.2': 'es-data-162',
    '172.16.16.3': 'es-data-163',
    '172.16.16.4': 'es-data-164',
}

hosts = '127.0.0.1\tlocalhost\n::1\tlocalhost\n'
addrs = list(set([i[4][0] for i in socket.getaddrinfo(socket.gethostname(), None, socket.AF_INET)]))
ip = ''
name = ''

for k, v in master_nodes.items():
    hosts += '%s\t%s\n' % (k, v)
for k, v in data_nodes.items():
    hosts += '%s\t%s\n' % (k, v)
open('/etc/hosts','w').write(hosts)
print('Writing hosts')


print('Writing sysctl')
sysctl = open('/etc/sysctl.conf').read().strip()
if 'max_map_count' not in sysctl:
    sysctl += '\nvm.max_map_count = 655360'
open('/etc/sysctl.conf','w').write(sysctl)
os.system('sysctl -p')

print('Writing limits')
os.system('cp limits.conf /etc/security/limits.conf')

print('Writing jvm options')
os.system('sed -i "s/-Xms1g/-Xms2g/g" /opt/elasticsearch-6.4.0/config/jvm.options')
os.system('sed -i "s/-Xmx1g/-Xmx2g/g" /opt/elasticsearch-6.4.0/config/jvm.options')

master = None
for i in addrs:
    if i in master_nodes.keys():
        ip, name = i, master_nodes[i]
        master = True
        break
    if i in data_nodes.keys():
        ip, name = i, data_nodes[i]
        master = False
        break
print('Self IP: %s\tName: %s' % (ip, name))

if master:
    data = open('master.yml').read().replace('%ip%', ip).replace('%name%', name)
else:
    data = open('data.yml').read().replace('%ip%', ip).replace('%name%', name)
open('/opt/elasticsearch-6.4.0/config/elasticsearch.yml', 'w').write(data)
print('Writing Elasticsearch Config')
